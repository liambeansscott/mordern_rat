import React from 'react';
import { Switch, BrowserRouter, Route } from 'react-router-dom';
import Gallery from './Pages/Gallery';
import Rat from './Pages/Rat/Rat';
import Shop from './Pages/Shop/Shop';
import Basket from './Pages/Basket/Basket'
import Home from './Pages/Home/Home'
import Layout from './Components/Layout/layout'
import Checkout from './Pages/Checkout/Checkout'

const App = ( props: any ) => {
  return (
    <BrowserRouter>
      <Layout>
        <Switch>
          <Route exact path="/">
            <Home />
          </Route>
          <Route path="/rat">
            <Rat />
          </Route>
          <Route path="/shop">
            <Shop />
          </Route>
          <Route path="/gallery">
            <Gallery />
          </Route>
          <Route path="/basket">
            <Basket />
          </Route>
          <Route path="/checkout" render={routerProps => <Checkout {...routerProps} />} />

        </Switch>
      </Layout>
    </BrowserRouter>
  )
}


export default App;
