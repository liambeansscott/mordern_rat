export interface ShopItemProps {
    name: string,
    price: number,
    description: string,
    id: string,
    detail: string,
    addToBasket: Function,
}
export interface ShopItemDetails {
    name: string,
    price: number,
    description: string,
    id: string,
    detail: string,
}

export interface State {
    activePage: string,
    basket: ShopItemDetails[]
}
export type FormControlOptionValue = "SELECT" | "INPUT";

export interface FormControlProps {
    inputType: FormControlOptionValue;
    label: string;
    doubleWidth?: boolean;
    errorMsg?: string;
    id?: string;
    hook: Function;
}

export interface FormControlOption {
    INPUT: FormControlOptionValue,
    SELECT: FormControlOptionValue,
}

export interface StyleProps {
    root: string,
}