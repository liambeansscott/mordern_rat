import { createMuiTheme } from '@material-ui/core';
import 'fontsource-roboto';
const theme = createMuiTheme( {
    palette: {
        background: { default: "#0F0B08" },
        primary: { main: "#FDFAFA", },
        secondary: { main: "#707070", }
    },
    typography: {
        fontFamily: "system-ui",

        h1: {
            fontSize: "9rem",
            fontWeight: 100,
            letterSpacing: "0",
            lineHeight: "0.8em",
            gutterBottom: {
                marginBottom: "0.7em",
            }
        },
        h2: {
            fontSize: "7rem",
            fontWeight: 100,
            letterSpacing: "0",
            lineHeight: "1.2em",
            gutterBottom: {
                marginBottom: "0.7em",
            }

        },
        body1: {
            fontSize: "3rem",
            fontWeight: 100,
            lineHeight: "3rem",
            gutterBottom: {
                marginBottom: "0.7em",
            }
        },
        body2: {
            fontSize: "2rem",
            fontWeight: 100,
            lineHeight: "3rem"
            , gutterBottom: {
                marginBottom: "0.7em",
            }
        }
    },

} )

export default theme;