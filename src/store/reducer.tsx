import * as actionTypes from './actions';
import { State } from '../interfaces'

const initialState: State = {
    activePage: "",
    basket: [],
}

const reducer = ( state = initialState, action: any ) => {
    switch ( action.type ) {
        case actionTypes.SET_ACTIVE_PAGE:
            return { ...state, activePage: action.payload }
        case actionTypes.ADD_ITEM_TO_BASKET:
            console.log( 'ADD ITEM TO BASKET', state.basket, action.payload )
            return { ...state, basket: [...state.basket, action.payload] }
        default:
            return state

    }
}

export default reducer;