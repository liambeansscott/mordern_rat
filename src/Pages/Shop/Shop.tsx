import React, { useState, useEffect } from 'react'
import MenuItem from '../../Components/Menu/MenuItem/menuItem'
import * as s from './styled'
import { Route, withRouter } from 'react-router-dom'
import ShopItem from '../../Components/Shopitem/shopItem'
import { connect } from 'react-redux'
import { ShopItemDetails } from '../../interfaces'
import * as actionTypes from '../../store/actions'


const mockShopItems: ShopItemDetails[] = [
    {
        name: "ACAB",
        price: 10,
        description: "A3 lino print",
        id: "1",
        detail: "all proceeds to Inquest charity",
    },
    {
        name: "Matador",
        price: 15,
        description: "A3 ink painting",
        id: "2",
        detail: "the bull wins",
    },
    {
        name: "Dawg",
        price: 12,
        description: "A4 ink and graphite drawing",
        id: "3",
        detail: "a page from my upcoming dawg book",
    },
]
const Shop = ( props: any ) => {
    const [activeShopItemId, setActiveShopItemId] = useState<string | null>( null );
    const [activeShopItem, setActiveShopItem] = useState<ShopItemDetails | null>( null );
    const mockFn = () => { return false }
    const shopItems = mockShopItems.map(
        shopItem => <MenuItem
            vertical={true}
            link={`/shop/item/${shopItem.id}`}
            pageName={shopItem.name}
            onSetActivePage={() => setActiveShopItemId( shopItem.id )}
            setBackgroundImage={mockFn} /> )
    useEffect( () => {
        setActiveShopItem( mockShopItems.filter( mockshopItem => mockshopItem.id === activeShopItemId )[0] )
    }, [activeShopItemId] )
    console.log( props.match )
    return (
        <s.Container>
            <Route path={`${props.match.path}/:shopItemId`} >
                <MenuItem vertical={true} pageName={"Shop"} link={`${props.match.path}`} onSetActivePage={() => setActiveShopItemId( null )} setBackgroundImage={mockFn} />
                {activeShopItem && <ShopItem
                    name={activeShopItem.name}
                    price={activeShopItem.price}
                    description={activeShopItem.description}
                    id={activeShopItem.id}
                    detail={activeShopItem.description}
                    addToBasket={props.onAddItemToBasket}
                />}
            </Route>
            <Route exact path={`${props.match.path}`}>
                <MenuItem vertical={true} pageName={"Basket"} link={"/basket"} onSetActivePage={mockFn} setBackgroundImage={mockFn} />
                {shopItems}
            </Route>

        </s.Container >

    )

}
const mapDispatchToProps = ( dispatch: any ) => {
    return {
        onAddItemToBasket: ( shopItem: ShopItemDetails ) => dispatch( { type: actionTypes.ADD_ITEM_TO_BASKET, payload: shopItem } )
    }
}
export default connect( null, mapDispatchToProps )( withRouter( Shop ) );
