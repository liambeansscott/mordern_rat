import React from 'react';
import { formControlOption } from '../../Components/FormControl/formControl'
import * as s from './styled';
import MenuItem from '../../Components/Menu/MenuItem/menuItem'
import { Route } from 'react-router-dom'
import AddressForm from '../../Components/Forms/AddressForm/AddressForm';
import { connect } from 'react-redux';
import * as actions from '../../store/actions'

const Checkout: React.FC<any> = ( props: any ) => {
    const mock = () => { };
    const addressForm: any = {
        country: {
            value: "",
            required: true,
            inputType: formControlOption.SELECT,
            label: "Country",
            doubleWidth: true,

        },
        firstName: {
            value: "",
            required: true,
            inputType: formControlOption.INPUT,
            label: "First Name",
        },
        lastName: {
            value: "",
            required: true,
            inputType: formControlOption.INPUT,
            label: "Last Name",


        },
        addressLine1: {
            value: "",
            required: true,
            inputType: formControlOption.INPUT,
            label: "Address Line 1",
            doubleWidth: true,

        },
        addressLine2: {
            value: "",
            required: true,
            inputType: formControlOption.INPUT,
            label: "Address Line 2",
            doubleWidth: true,
        },
        city: {
            value: "",
            required: true,
            inputType: formControlOption.INPUT,
            label: "City",


        },
        county: {
            value: "",
            required: false,
            inputType: formControlOption.INPUT,
            label: "County",


        },
        postCode: {
            value: "",
            required: true,
            inputType: formControlOption.INPUT,
            label: "Post Code",


        },
        phoneNumber: {
            value: "",
            required: true,
            inputType: formControlOption.INPUT,
            label: "Phone Number",


        }
    }

    return (
        <s.Container >
            <Route exact path={`${props.match.path}`}>
                <MenuItem
                    vertical={false}
                    setBackgroundImage={mock}
                    link={`${props.match.path}/address`}
                    pageName="Address"
                    onSetActivePage={() => props.onSetActivePage( "Address" )}
                />
                <MenuItem
                    vertical={false}
                    setBackgroundImage={mock}
                    link="/Payment"
                    pageName="Payment"
                    onSetActivePage={mock}
                    disabled={true}
                />
                <MenuItem
                    vertical={false}
                    setBackgroundImage={mock}
                    link="/Review"
                    pageName="Review"
                    onSetActivePage={mock}
                    disabled={true}
                />
            </Route>
            <Route exact path={`${props.match.path}/address`}>
                <div style={{ display: "flex" }}>
                    <div style={{ flex: 1 }}>
                        <AddressForm />
                    </div>

                    <div style={{ flex: 1 }}>
                        {props.basket}
                        <MenuItem
                            vertical={false}
                            setBackgroundImage={mock}
                            link="/checkout"
                            pageName="checkout"
                            onSetActivePage={() => props.onSetActivePage( "Checkout" )}
                        />
                    </div>
                </div>
            </Route>

        </s.Container >
    )
}
const mapDispatchToProps = ( dispatch: any ) => {
    return {
        onSetActivePage: ( name: string ) => dispatch( { type: actions.SET_ACTIVE_PAGE, payload: name } )
    }
}

const mapStateToProps = ( state: any ) => {
    return {
        basket: state.basket
    }
}
export default connect( mapStateToProps, mapDispatchToProps )( Checkout );