import styled from 'styled-components'

export const Wrapper = styled.div`
    display: flex;
    flex-direction:row;
    height:100%;
    width:100%;


`
export const ContactData = styled.div`
    letter-spacing: 0;
    flex:1;
    margin:auto 0 auto 0;
    margin-left:4rem;
`

export const StyledImage = styled.img`
    width:100%;
    max-height:auto;
    object-fit:contain;
`
export const ImgWrapper = styled.div`
    height:100%;
    flex:1;
    justify-content:end;
    overflow:hidden;


`