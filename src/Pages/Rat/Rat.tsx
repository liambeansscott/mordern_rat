import React from 'react';
import { ContactData, Wrapper, StyledImage, ImgWrapper } from './styled';
import img from '../../assets/enquiryImage.png'
import { Typography } from '@material-ui/core'
const Rat = () => {
    return (
        <Wrapper>

            <ContactData>
                <Typography variant="body1" color="secondary">
                    A front-end developer and visual artist
                </Typography>
                <Typography variant="body2" color="primary">
                    {'liamscott666@icloud.com'}
                </Typography>
                <Typography variant="body2" color="primary">
                    {'@modern.rat'}
                </Typography>
            </ContactData>
            <ImgWrapper>
                <StyledImage src={img}></StyledImage>
            </ImgWrapper>

            {/* </Wrapper> */}

        </Wrapper >
    )

}
export default Rat;