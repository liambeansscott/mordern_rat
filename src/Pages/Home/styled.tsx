import styled from 'styled-components'

interface MenuProps {
    backgroundImage: string | null;
}
export const Menu = styled.div <MenuProps> `
    display:flex;
    flex-direction: column;
    justify-content: center;
    height:100%;
    background: ${p => `url(${p.backgroundImage}) no-repeat top center`};
    overflow: scroll;
`


