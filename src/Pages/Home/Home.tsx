import React, { useState } from 'react'
import { Menu } from './styled'
import MenuItem from '../../Components/Menu/MenuItem/menuItem'
import { connect } from 'react-redux'
import * as actions from '../../store/actions'
import imgRat from '../../assets/enquiryImage.png'
import imgGall from '../../assets/galleryImage.png'
import imgShop from '../../assets/shopImage.png'
const Home = ( props: any ) => {
    const [backgroundImage, setBackgroundImage] = useState<string | null>( null )
    return (
        <Menu backgroundImage={backgroundImage}>
            <MenuItem
                vertical={false}
                setBackgroundImage={setBackgroundImage}
                link="/rat"
                pageName="Rat"
                backgroundImg={imgRat}
                onSetActivePage={props.onSetActivePage}
            />
            <MenuItem
                vertical={false}
                setBackgroundImage={setBackgroundImage}
                link="/shop"
                pageName="Shop"
                backgroundImg={imgShop}
                onSetActivePage={props.onSetActivePage}
            />
            <MenuItem
                vertical={false}
                setBackgroundImage={setBackgroundImage}
                link="/gallery"
                pageName="Gallery"
                backgroundImg={imgGall}
                onSetActivePage={props.onSetActivePage}
            />


        </Menu>
    )
}
const mapDispatchToProps = ( dispatch: any ) => {
    return {
        onSetActivePage: ( activePage: string ) => {
            return dispatch( { type: actions.SET_ACTIVE_PAGE, payload: activePage } )
        }
    }
}
export default connect( null, mapDispatchToProps )( Home ); 