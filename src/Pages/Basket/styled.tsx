import styled from 'styled-components'
export const Container = styled.div`
display: flex;
flex-direction:row;
height:100%;
box-sizing:border-box;

`
export const Basket = styled.div`
    height:100%;
    flex:6;
    display: flex;
    flex-direction: column;
`

export const BasketItems = styled.div`
height:auto;
overflow: scroll;
border: 1px gray solid;
`
export const OrderTotal = styled.div`
    height:auto;
    margin:1em auto;
`