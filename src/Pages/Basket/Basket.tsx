import React, { ReactElement } from 'react';
import { connect } from 'react-redux'
import { State, ShopItemDetails } from '../../interfaces'
import MenuItem from '../../Components/Menu/MenuItem/menuItem'
import { Typography } from '@material-ui/core'
import * as s from './styled'
interface BasketProps {
    basket: ShopItemDetails[]
}
const mockBasket: ShopItemDetails[] = [
    {
        name: "ACAB",
        price: 10,
        description: "A3 lino print",
        id: "1",
        detail: "all proceeds to Inquest charity",
    },
    {
        name: "Matador",
        price: 15,
        description: "A3 ink painting",
        id: "2",
        detail: "the bull wins",
    },
    {
        name: "Dawg",
        price: 12,
        description: "A4 ink and graphite drawing",
        id: "3",
        detail: "a page from my upcoming dawg book",
    },
    {
        name: "Dawg",
        price: 12,
        description: "A4 ink and graphite drawing",
        id: "3",
        detail: "a page from my upcoming dawg book",
    },
    {
        name: "Dawg",
        price: 12,
        description: "A4 ink and graphite drawing",
        id: "3",
        detail: "a page from my upcoming dawg book",
    },
    {
        name: "Dawg",
        price: 12,
        description: "A4 ink and graphite drawing",
        id: "3",
        detail: "a page from my upcoming dawg book",
    },
]
const Basket: React.FC<BasketProps> = ( props: BasketProps ) => {
    const basketItems: ReactElement[] = props.basket.map( basketItem =>
        <Typography variant="h2" color="secondary">{`${basketItem.name} £${basketItem.price}`}</Typography>

    )
    let subTotal: number = props.basket.reduce( ( total: number, basketItem ) => { return total + basketItem.price }, 0 )
    return (
        <s.Container>
            <MenuItem vertical={true} link={"/shop"} pageName={"Shop"} onSetActivePage={() => { }} setBackgroundImage={() => { }} />
            <s.Basket>
                <s.BasketItems>
                    {basketItems}
                </s.BasketItems>
                <Typography variant="h2" color="primary">
                    {`Subtotal:   £${subTotal}`}
                </Typography>
                <s.OrderTotal>

                </s.OrderTotal>
                <MenuItem vertical={false} link={"/checkout"} pageName={"Checkout"} onSetActivePage={() => { }} setBackgroundImage={() => { }} />

            </s.Basket>
        </s.Container> )
}
const mapStateToProps = ( state: State ) => {
    return {
        // basket: state.basket
        basket: mockBasket
    }
}
export default connect( mapStateToProps )( Basket );