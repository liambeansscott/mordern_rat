import React from 'react'
import { Button, makeStyles, Theme } from '@material-ui/core'
import useForm from '../../Hooks/useForm'
import FormControl, { formControlOption } from '../FormControl/formControl'

const useStyles = makeStyles( ( theme: Theme ) => ( {
    root: {
        margin: "1em",
        "& .MuiSelect-selectMenu": {
            height: 0,
        },
        "& .MuiOutlinedInput-root .MuiOutlinedInput-notchedOutline": {
            borderColor: theme.palette.secondary.main,
        },

        "& .MuiOutlinedInput-root.Mui-focused .MuiOutlinedInput-notchedOutline": {
            borderColor: theme.palette.primary.main,
        },
        "& .MuiOutlinedInput-input": {
            color: theme.palette.primary.main,
            fontSize: "1.4rem",
        },
        "& .MuiOutlinedInput-root.Mui-focused .MuiOutlinedInput-input": {
            color: theme.palette.primary.main,
        },
        "& .MuiInputLabel-outlined": {
            color: theme.palette.secondary.main,
            fontSize: "1.4rem",

        },
        "&:hover .MuiInputLabel-outlined": {
            color: theme.palette.primary.main,
        },
        "& .MuiInputLabel-outlined.Mui-focused": {
            color: theme.palette.primary.main,
        }
    }
} ) )


const formControls: any[] = [
    {
        label: 'firstName',
        inputType: formControlOption.INPUT,
        doubleWidth: false,

        pattern: {
            value: /^\w{3,50}$/,
            message: 'invalid name'

        }
    },
    {
        label: 'lastName',
        doubleWidth: false,
        inputType: formControlOption.INPUT,
        pattern: {
            value: /^\w{3,50}$/,
            message: 'invalid name'

        }
    }, {
        label: 'addressLine1',
        doubleWidth: true,
        inputType: formControlOption.INPUT,
        pattern: {
            value: /^[a-zA-Z0-9 ]{4,99}$/,
            message: 'invalid address'

        }
    },
    {
        label: 'addressLine2',
        doubleWidth: true,
        inputType: formControlOption.INPUT,
        pattern: {
            value: /^[a-zA-Z0-9 ]{4,99}$/,
            message: 'invalid address'

        }
    },
    {
        label: 'postcode',
        doubleWidth: true,
        inputType: formControlOption.INPUT,
        pattern: {
            value: /^[a-zA-Z0-9 ]{6,7}$/,
            message: 'invalid postcode'

        }
    },


]
interface SimpleFormProps {
}
const SimpleForm: React.FC<SimpleFormProps> = ( props: SimpleFormProps ) => {
    let validations = {}
    for ( let formControl of formControls ) {
        validations = {
            ...validations,
            [formControl.label]: {
                pattern: formControl.pattern
            }
        }
    }
    const { bindField, values, errors, isValid } = useForm( {
        validations: validations

    } )
    const formControlElements: any[] = formControls.map(
        formControl => <FormControl
            errorMsg={errors[formControl.label]}
            label={formControl.label}
            inputType={formControl.inputType}
            hook={() => bindField( formControl.label )}
        />
    )

    return (
        <>
            {formControlElements}
            <Button disabled={!isValid()} variant="outlined" color="primary" >submit</Button>

        </>
    );
}

export default SimpleForm;