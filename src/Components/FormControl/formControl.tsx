import React from 'react'
import { MenuItem, TextField, Theme } from '@material-ui/core'
import { makeStyles } from '@material-ui/core'
import { FormControlProps, FormControlOption, StyleProps } from '../../interfaces'

const useStyles: Function = makeStyles( ( theme: Theme ) => ( {
    root: {
        margin: "1em",
        "& .MuiSelect-selectMenu": {
            height: 0,
        },
        "& .MuiOutlinedInput-root .MuiOutlinedInput-notchedOutline": {
            borderColor: theme.palette.secondary.main,
        },

        "& .MuiOutlinedInput-root.Mui-focused .MuiOutlinedInput-notchedOutline": {
            borderColor: theme.palette.primary.main,
        },
        "& .MuiOutlinedInput-input": {
            color: theme.palette.primary.main,
            fontSize: "1.4rem",
        },
        "& .MuiOutlinedInput-root.Mui-focused .MuiOutlinedInput-input": {
            color: theme.palette.primary.main,
        },
        "& .MuiInputLabel-outlined": {
            color: theme.palette.secondary.main,
            fontSize: "1.4rem",

        },
        "&:hover .MuiInputLabel-outlined": {
            color: theme.palette.primary.main,
        },
        "& .MuiInputLabel-outlined.Mui-focused": {
            color: theme.palette.primary.main,
        }
    }
} ) )


export const formControlOption: FormControlOption = {
    SELECT: "SELECT",
    INPUT: "INPUT"
}

const FormControl: React.FC<FormControlProps> = ( props: FormControlProps ) => {
    const classes: StyleProps = useStyles()
    return (
        <>
            {props.inputType === formControlOption.SELECT &&

                <TextField size="small" fullWidth variant="outlined" className={classes.root} select label={`${props.label}`}>
                    <MenuItem value="">
                        <em>Country</em>
                    </MenuItem>
                    <MenuItem value={10}>Ten</MenuItem>
                    <MenuItem value={20}>Twenty</MenuItem>
                    <MenuItem value={30}>Thirty</MenuItem>
                </TextField>
            }
            {

                props.inputType === formControlOption.INPUT &&
                <TextField
                    error={Boolean( props.errorMsg )}
                    helperText={props.errorMsg}
                    className={classes.root}
                    id={props.id}
                    fullWidth
                    size="small"
                    color="primary"
                    label={`${props.label}`}
                    variant="outlined"
                    {...props.hook()}
                />
            }


        </ >
    )
}
export default FormControl