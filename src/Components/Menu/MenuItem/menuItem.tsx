import React from 'react'
import * as s from './styled'
import { NavLinkStyled } from '../../../globalStyles'
import { Typography } from '@material-ui/core'
interface MenuItemProps {
    onSetActivePage: Function;
    setBackgroundImage: Function;
    link: string;
    pageName: string;
    backgroundImg?: string;
    vertical: boolean;
    disabled?: boolean;

}
const MenuItem: React.FC<MenuItemProps> = ( props: MenuItemProps ) => {

    return (
        <s.MenuItem
            vertical={props.vertical}
            onMouseEnter={() => props.setBackgroundImage( props.backgroundImg )}
            onMouseLeave={() => props.setBackgroundImage( null )}
            disabled={props.disabled}
        >
            <NavLinkStyled
                onClick={() =>
                    props.onSetActivePage( props.pageName )
                } to={props.link}>
                <Typography variant="h1">
                    {props.pageName}
                </Typography>
            </NavLinkStyled>
        </s.MenuItem>

    );
}
export default MenuItem;