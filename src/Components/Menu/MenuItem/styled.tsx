import styled from 'styled-components'
import { NavLinkStyled, colours } from '../../../globalStyles'
interface MenuItemProps {
    vertical?: boolean;
    disabled?: boolean;
}

export const MenuItem = styled.div<MenuItemProps>`
    border-bottom: solid 1px ${ colours.darkGray};

    border-bottom: ${p => p.vertical ? "none" : `solid 1px ${colours.darkGray}`};
    border-right: none;

    border-right: ${p => p.vertical ? `solid 1px ${colours.darkGray}` : "none"};
    color: ${colours.lightGray};
    color: ${p => p.disabled ? colours.darkGray : colours.lightGray};
    flex:1;
    display:flex;
    ${NavLinkStyled}{
        display:flex;
        align-items:center;
        margin:${p => p.vertical ? "auto" : "0 0 0 0.7em"};;
        writing-mode: ${p => p.vertical ? "vertical-rl" : "auto"};
        text-orientation: ${p => p.vertical ? "upright" : "auto"};
        letter-spacing: ${p => p.vertical ? "-0.3em" : "auto"};
        text-align:${p => p.vertical ? "center" : "auto"};
        font-size: ${p => p.vertical ? "5.3rem" : "inherit"}

    }
`
