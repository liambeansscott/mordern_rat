import { colours } from '../../globalStyles';
import styled from 'styled-components'

export const Layout = styled.div`
    box-sizing:border-box;
    height:100%;
    width:100vw;
    letter-spacing: 0.1em;
    background-color: ${colours.offBlack};
    display: grid;
    grid-template-columns: min-content 1fr;
    grid-template-rows: min-content 1fr;
    grid-template-areas: 
    "header header"
    "sidedrawer content";
`
export const Banner = styled.div`
    font-weight:200;
    text-decoration: "none";
    grid-area: header;
    display: block;
    border-bottom: 1px ${colours.darkGray} solid;
    padding:2em;
`
export const Sidedrawer = styled.div`
    box-sizing:border-box;
    font-weight:inherit;
    grid-area: sidedrawer;
    padding-top:1em;
    color: ${colours.darkGray};
    border-right: 1px ${colours.darkGray} solid;
    padding-top:14em;
`
export const Page = styled.div`
    box-sizing:border-box;
    grid-area: content;
    height:100%;
    overflow:hidden;
`
export const AnnotationLetter = styled.p`
    margin:0;
    display: inline;
`