import React, { ReactElement } from 'react'
import { LinkStyled } from '../../globalStyles'
import { connect } from 'react-redux'
import * as actions from "../../store/actions"
import { Layout, Banner, Sidedrawer, Page } from './styled'
import { Typography } from '@material-ui/core'

interface LayoutProps {
    activePage: string,
    onSetActivePage: Function,
    children: ReactElement
}
const Layouts: React.FC<LayoutProps> = ( props: LayoutProps ) => {

    const annotationLetters: JSX.Element[] | null | any =
        props.activePage ?
            props.activePage.split( "" ).map( ( letter: string, index: number ) =>
                <div key={index}
                    style={{
                        overflow: "hidden",
                        width: "auto",
                        display: 'inline',
                    }}>
                    <Typography variant="h1" color="primary" display="inline">{letter}</Typography>
                </div >
            ) : null;
    return (
        <Layout>
            <Banner>
                <Typography variant="h1" color="secondary" display="inline" >{`Modern \t`}</Typography>
                {annotationLetters}
            </Banner>
            <Sidedrawer>
                <LinkStyled onClick={() => props.onSetActivePage( "" )} to="/">
                    <Typography variant="h1" color="secondary" >+</Typography>
                </LinkStyled>
            </Sidedrawer>
            <Page>
                {props.children}
            </Page>
        </Layout >

    )
}
const mapStateToProps = ( state: any ) => {
    return {
        activePage: state.activePage
    }
}
const mapDispatchToProps = ( dispatch: any ) => {
    return {
        onSetActivePage: ( name: string ) => dispatch( { type: actions.SET_ACTIVE_PAGE, payload: name } )
    }
}
export default connect( mapStateToProps, mapDispatchToProps )( Layouts );