import styled from 'styled-components'

export const Container = styled.div`
    box-sizing:border-box;
    height:100%;
    margin: 2em;
    display: grid;
    grid-template-columns: repeat(2, 1fr);
    grid-template-rows: repeat(8, 1fr);
    grid-column-gap:2em;
`

export const doubleWidth = styled.div`
    grid-column: auto/span 2;
`