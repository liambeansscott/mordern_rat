import React, { ReactElement } from 'react'
import * as s from './styled'
import FormControl, { formControlOption } from '../../FormControl/formControl'
import useForm from '../../../Hooks/useForm'
import { FormControlOptionValue } from '../../../interfaces'

const formControlOptions: FormControlOption[] = [
    {
        label: 'firstName',
        inputType: formControlOption.INPUT,
        doubleWidth: false,

        pattern: {
            value: /^\w{3,50}$/,
            message: 'invalid name'

        }
    },
    {
        label: 'lastName',
        doubleWidth: false,
        inputType: formControlOption.INPUT,
        pattern: {
            value: /^\w{3,50}$/,
            message: 'invalid name'

        }
    }, {
        label: 'addressLine1',
        doubleWidth: true,
        inputType: formControlOption.INPUT,
        pattern: {
            value: /^[a-zA-Z0-9 ]{4,99}$/,
            message: 'invalid address'

        }
    },
    {
        label: 'addressLine2',
        doubleWidth: true,
        inputType: formControlOption.INPUT,
        pattern: {
            value: /^[a-zA-Z0-9 ]{4,99}$/,
            message: 'invalid address'

        }
    },
    {
        label: 'postcode',
        doubleWidth: true,
        inputType: formControlOption.INPUT,
        pattern: {
            value: /^[a-zA-Z0-9 ]{6,7}$/,
            message: 'invalid postcode'

        }
    },
]
interface FormControlOption {
    label: string,
    doubleWidth: boolean,
    inputType: FormControlOptionValue,
    pattern: {
        value: RegExp,
        message: string,
    }

}



interface FormProps {
    // formControlOptions: FormControlOption[]
}
const AddressForm: React.FC<FormProps> = ( props: FormProps ) => {
    let validations = {}
    for ( let formControl of formControlOptions ) {
        validations = {
            ...validations,
            [formControl.label]: {
                pattern: formControl.pattern
            }
        }
    }
    const { bindField, errors } = useForm( {
        validations: validations
    } )
    const formControls: ReactElement[] = formControlOptions
        .map( ( formControlOption: FormControlOption ) => {
            let formControl: ReactElement = <FormControl
                errorMsg={errors[formControlOption.label]}
                hook={() => bindField( formControlOption.label )}
                {...formControlOption}
            />
            return formControlOption.doubleWidth ? <s.doubleWidth>{formControl}</s.doubleWidth> : formControl
        }
        )

    return (
        <s.Container>
            {formControls}
        </s.Container>
    )
}

export default AddressForm