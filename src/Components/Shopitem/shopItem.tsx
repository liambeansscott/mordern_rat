import React from 'react';
import shopImage from '../../assets/shopImage.png'
import * as s from './styled'
import { ShopItemProps, StyleProps } from '../../interfaces'
import { Typography, Button } from '@material-ui/core'
import { makeStyles } from '@material-ui/core'
const useStyles = makeStyles( {
    root: {
        buttonStyle:
        {
            fontSize: '3em', fontWeight: 'inherit',
            fontFamily: 'system-ui',
            margin: 0,
        }
    }


} )
const ShopItem = ( props: ShopItemProps ) => {
    const classes: StyleProps = useStyles()

    return (
        < s.Container >
            <s.ItemInfo>

                <Typography variant="h1" color="primary">
                    {props.name}
                </Typography>
                <Typography variant="body2" color="secondary" gutterBottom
                    style={{ marginBottom: "1.2em" }} >
                    {props.description}
                </Typography>
                <Typography variant="body1" color="secondary" style={{ marginBottom: "1.2em" }} gutterBottom>
                    {props.detail}
                </Typography>
                <Typography style={{ marginBottom: "0.2em" }} variant="h1" color="primary" gutterBottom>
                    {`£${props.price}`}
                </Typography>
                <Button
                    className={classes.root}
                    variant="outlined"
                    color="primary"
                    size="large"
                    onClick={() => props.addToBasket( {
                        name: props.name,
                        detail: props.detail,
                        price: props.price,
                        description: props.description,

                    } )}
                >
                    Add to cart
                </Button>

            </s.ItemInfo>
            <s.ItemImageContainer>
                <img src={shopImage} />
            </s.ItemImageContainer>
        </s.Container >
    )
}

export default ShopItem