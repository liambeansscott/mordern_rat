import styled from 'styled-components'
export const Container = styled.div`
    overflow: hidden; 
    display: flex; 
    margin: 3em 7em; 

`
export const ItemInfo = styled.div`
    padding: 0.2rem;
    flex: 1;  
    overflow:hidden;
    `

export const ItemImageContainer = styled.div`
    flex: 1;
    img{ 
        width: 100%;
        height: auto; 
    } `
