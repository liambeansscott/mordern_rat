import styled from 'styled-components'
import { Link, NavLink } from 'react-router-dom'
export const colours = {
    offBlack: "#0F0B08",
    darkGray: "#707070",
    lightGray: "#D2CFC6",
    offWhite: "#FDFAFA",
}

export const LinkStyled = styled( Link )`
    text-decoration: none;

    &:focus, &:hover, &:visited, &:link, &:active {
        text-decoration: none;
    }
    color: inherit;
`;
export const NavLinkStyled = styled( NavLink )`
    text-decoration: none;

    &:focus, &:hover, &:visited, &:link, &:active {
        text-decoration: none;
    }
    color: inherit;
`;