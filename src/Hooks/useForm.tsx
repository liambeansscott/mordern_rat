import { useState } from 'react'
export const errors = {
    required: 'the option `validations` is required',
    type: 'the option `validations` should be an object',
}

export interface Validation {
    required: string,
    pattern: string,
}

const UseForm: Function = ( { validations, initialValues = {} }: any, ) => {
    if ( typeof initialValues !== 'object' ) {
        throw new Error( 'the option `initialValues` should be an object' );
    }
    const [values, setValues] = useState<any>( initialValues );

    // const [values, setValues] = useState<any>( {} );
    const [errors, setErrors] = useState<any>( {} );

    const bindField = ( name: string ) => {
        if ( !name ) {
            throw new Error( 'The field name parameter is required' )
        }
        if ( name && typeof name !== 'string' ) {
            throw new Error( 'The field name should be a string' )
        }

        return {
            value: values[name] || '',
            onChange: ( e: any ) => {
                console.log( e )
                const { value } = e.target;

                setValues( ( state: any ) => ( {
                    ...state,
                    [name]: value,
                } ) );

                setErrors( ( state: any ) => ( {
                    ...state,
                    [name]: validateField( name, value ),
                } ) );
            },
        }
    };
    const validateField = ( name: any, value: any ) => {
        const rules = validations[name];
        if ( rules ) {
            if ( rules.required ) {
                if ( !value.trim() ) {
                    return typeof rules.required === 'string' ? rules.required : 'required';
                }
            }
            if ( rules.pattern ) {
                if ( !new RegExp( rules.pattern.value ).exec( value ) ) {
                    // if the value does not match with the regex pattern, we try to return
                    // the custom message and fallback to the default message in case
                    return rules.pattern.message || 'invalid';
                }
            }
            if ( rules.validate && typeof rules.validate === 'function' ) {
                // we run the validate function with the field value
                const error = rules.validate( value );

                // if an error message was returned, we return it
                if ( error ) {
                    return error;
                }
            }
        }
        return ''
    };

    if ( !validations ) {
        console.log( 'required' )
        throw new Error( 'the option `validations` is required' );
    }

    if ( typeof validations !== 'object' ) {
        console.log( 'object' )

        throw new Error( 'the option `validations` should be an object' );
    }
    const isValid = () => {
        const hasErrors = Object.keys( validations ).some( name =>
            Boolean( validateField( name, values[name] ) ) );
        return !hasErrors;
    }
    return {
        validateField,
        values,
        errors,
        bindField,
        isValid
    }
}
export default UseForm